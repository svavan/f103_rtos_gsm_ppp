#include "user_tasks.h"

//=======================================================================================

sBuff buff = {0};

//===========================================================================================================
void StartThread(void * argument)
{
     gsmTaskInit();
     // ����������, ��������/����� ������
     xTaskCreate(connectTask, "connectTask", configMINIMAL_STACK_SIZE*1, 0, tskIDLE_PRIORITY+1, NULL);
     // ����� ������� ������
     vTaskDelete(NULL);
}
//===========================================================================================================
void gsmTaskInit(void)
{
     xTaskCreate(vGsmTask, "GSM", configMINIMAL_STACK_SIZE*2, 0, tskIDLE_PRIORITY+1, NULL);

	 while((gsmState.init != true) || (pppIsOpen != true))
	{
		vTaskDelay(100/portTICK_PERIOD_MS);
	}
}
//=======================================================================================
// ������ ������������� � ���������� GSM �������
void vGsmTask( void * pvParameters )
{
     // ��������������� �������������
     //GsmLLR_Init();
     //GsmLLR2_Init();
     //GsmPPP_Init();

	 // ���� ��������� �� ������

	 //while((gsmState.initLLR != true) && (gsmState.initLLR2 != true))
	 //{};

	 /*if(GsmLLR_PowerUp() != eOk)
	 {
          GsmLLR_ModuleLost();
     }*/

     for(;;)
		{
		  // �������������
		  if(gsmState.init == false)
		  {

				  // ���� ������ �������� ��������
				  if(gsmState.notRespond == true)
				  {
					   printf("GSM: INIT Module lost\r\n");
					   GsmLLR_ModuleLost();
					   continue;
				  }

				  // ���������� ������
				  if(GsmLLR_ATAT() != eOk)
				  {
					   gsmState.notRespond = true;
					   continue;
				  }

				  // ���������� �������������� �� �������
				  if(GsmLLR_WarningOff() != eOk)
				  {
					   gsmState.notRespond = true;
					   continue;
				  }

				  // ��������� ������
				  if(GsmLLR_FlowControl() != eOk)
				  {
					   gsmState.notRespond = true;
					   continue;
				  }

				  // ������ IMEI
				  /*
				  if(GsmLLR_GetIMEI(aIMEI) != eOk)
				  {
					   gsmState.notRespond = true;
					   continue;
				  }
				  DBGInfo("GSM: module IMEI=%s\r\n", aIMEI);

				  // ������ IMSI
				  if(GsmLLR_GetIMSI(aIMSI) != eOk)
				  {
					   gsmState.notRespond = true;
					   continue;
				  }
				  printf("GSM: module IMSI=%s\r\n", aIMSI);

				  // ������ Software
				  if(GsmLLR_GetModuleSoftWareVersion(aVerionSoftware) != eOk)
				  {
					   gsmState.notRespond = true;
					   continue;
				  }
                    */
				  // ����� ��������� � ����������� ���� (URC)
				  if(GsmLLR_AtCREG() != eOk)
				  {
					   gsmState.notRespond = true;
					   continue;
				  }
				printf("GSM: CREG OK\r\n");

				// ������ ������� �������

				if(false/*GsmLLR_UpdateCSQ(&gsmCsqValue) != eOk*/)
				{
				   printf("GSM: Get CSQ ERROR, -RELOAD\r\n");
				   gsmState.notRespond = true;
				   continue;
				}
				else
				{ /*
				   printf("GSM: CSQ value %d\r\n", gsmCsqValue);

				   // ������ SMS

				   if(GsmLLR_SmsModeSelect(sms_TEXT) != eOk)
				   {
						gsmState.notRespond = true;
						continue;
				   }

				   //������� sms
				   vTaskDelay(DELAY_REPLY_INIT/portTICK_RATE_MS);

				   if(GsmLLR_SmsClearAll() != eOk)
				   {
						printf("GSM: clear SMS ERROR, -RELOAD\r\n");
						gsmState.notRespond = true;
						continue;
				   }

					printf("GSM: Clear SMS Ok\r\n");
                    */
					printf("GSM: INIT PPPP\r\n");

					if(GsmLLR_StartPPP(&connectionSettings.gsmSettings) == eOk)
					{
						printf("GSM: INIT PPPP - PPP RUN\r\n");
						xQueueReset(uartParcerStruct.uart.rxQueue);
						uartParcerStruct.ppp.pppModeEnable = true;
						uartParcerStruct.uart.receiveState = true;
						gsmState.init = true;
					}
					else
					{
						printf("GSM: INIT PPPP - PPP ERROR!!!\r\n");
						gsmState.notRespond = true;
						continue;
					}
				}
			}
		vTaskDelay(1000/portTICK_RATE_MS);
     }
}

//============================================================================================================

void connectTask(void *pServiceNum)
{
     bool connectState = false;
     //eRetComm status = eError;
     uint16_t delay = 0;
     uint8_t serviceNum = *(uint8_t*)pServiceNum;
     xSemaphoreHandle xRxPppData; // ������� �� ����� �� ppp
     xRxPppData = GsmLLR_GetRxSemphorePoint(serviceNum);

     for(;;)
	 {
     /* �������� ��� ������ */
		 if(connectState == true)
		 {
			 //���� ���� ����������� � �������
			 while(GsmLLR_ConnectServiceStatus(serviceNum) == eOk)
			 {
				  //������ ������ � �����
				  buff[serviceNum].rxLen = getRxData(serviceNum, xRxPppData,&(buff[serviceNum].rxBuff));

				  if(buff[serviceNum].rxLen != 0)
				  {
							//���� ������ ������ �� ���������� �������
							if(GsmLLR_TcpSend(serviceNum, buff[serviceNum].rxBuff, buff[serviceNum].rxLen) == eOk)
							{
								printf("Connect:#%i SendData OK\r\n", serviceNum);
							}
							else
							{
								printf("Connect:#%i SendData ERROR\r\n", serviceNum);
								connectState = false;
							}
				  }
			 }

		 //���� ������ ����������
		 printf("Connect:#%i connection lost\r\n", serviceNum);
		 GsmLLR_DisconnectService(serviceNum);
		 connectState = false;
		 delay = 1000;
		 }
		 else
		 {

		 // ���������� �������, �����������
			 printf("Connect:#%i connecting...", serviceNum);

			 // ������������� ����������
			 if(GsmLLR_ConnectService(serviceNum) == eOk)
			 {
				  printf("Connect:#%i connected", serviceNum);
				  connectState = true;
			 }
			 else
			 { // �� ���������� �������������
				  printf("Connect:#%i ERROR", serviceNum);
				  delay = GSM_CONNECTION_ERROR_DELAY;
				  connectState = false;
			  }
		 }

		vTaskDelay(delay/portTICK_RATE_MS);
     }
}

//================================================================================================
//�� ���� ������ vGsmTask, �������, ��� � ������ ��������� ����������
//�GsmLLR_StartPPP� � ������������ ���� pppModeEnable � ��������� �������
//uartParcerStruct.uart.rxQueue. ���� pppModeEnable ���������� ������� ����� ������.
//����� ����� ����������� UART � ������/����������� ������ � ���� ����� �������.

//������ ������ PPP �� ������ GSM

void GsmPPP_Tsk(void *pvParamter)
{
	int timeout = 0;
	uint8_t i;
	bool stateInit = false;
	uint16_t tskStackInit;
	uint16_t pppNumport;
	LwipStack_Init();
	pppInit();
	pppSetAuth(PPPAUTHTYPE_CHAP, connectionSettings.gsmSettings.gprsUser, connectionSettings.gsmSettings.gprsPass);
	sioWriteSemaphore = xSemaphoreCreateBinary();

	for(i=0; i<GSM_MAX_CONNECTION; i++)
	{
		connectionPppStruct.semphr[i] = xSemaphoreCreateBinary();
		connectionPppStruct.rxData[i].rxSemh = xSemaphoreCreateBinary();
	}

	for(;;)
	{
        // ���� ��������� ���� � ������������� PPP � ��� ���� ���������
		if(uartParcerStruct.ppp.pppModeEnable == true)
		{
			if(!pppIsOpen)
			{
				pppNumport = pppOverSerialOpen(0, linkStatusCB, &pppIsOpen);
				pppStop = 0;
				timeout = 0;
				stateInit = false;

				while(timeout < 300)
				{
					if(pppIsOpen)
					{
						printf("PPP init - OK\r\n");
						lwip_stats.link.drop = 0;
						lwip_stats.link.chkerr = 0;
						lwip_stats.link.err = 0;
						stateInit = true;
						break;
					}
					else
					{
						timeout ++;
						vTaskDelay(100/portTICK_RATE_MS);
					}
				}
				if(stateInit != true)
				{
					printf("PPP init - TIMEOUT-ERROR\r\n");
					pppClose(pppNumport);
					pppIsOpen = false;
					uartParcerStruct.ppp.pppModeEnable = false;
					gsmState.init = false;
					gsmState.notRespond = true;
				}
			}
			else
			{
				if((lwip_stats.link.drop !=0) || (lwip_stats.link.chkerr !=0))
				{
					lwip_stats.link.drop = 0;
					lwip_stats.link.chkerr = 0;
					printf("GSMM: DROPING FAIL!!! RESTART PPP\r\n");

					for(i=0; i<SERVERS_COUNT; i++)
					{
						GsmPPP_Disconnect(i);
					}

					pppClose(pppNumport);
					pppIsOpen = false;
					uartParcerStruct.ppp.pppModeEnable = false;
					gsmState.init = false;
					gsmState.notRespond = true;
					vTaskDelay(500/portTICK_PERIOD_MS);
				}
			}
		}
		vTaskDelay(500/portTICK_RATE_MS);
	}
}

//========================================================================================

