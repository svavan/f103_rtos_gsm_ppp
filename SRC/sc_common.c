#include "sc_common.h"

extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;

//������� ����������� ������� LwIP � UART � GSM �������, ����� ������ ������

//===========================================================================================================
// ��������� �� ������ �� ������� - �������� ������ � LwIP
uint32_t sio_read(sio_fd_t fd, uint8_t *data, uint32_t len)
{
	unsigned long i = 0;

	if(uartParcerStruct.ppp.pppModeEnable)
	{
		while(xQueueReceive(uartParcerStruct.uart.rxQueue,&data[i], 0) == pdTRUE)
		{
			if(i==0)
			{
				printf("Reading PPP packet from UART\r\n");
			}

			printf("%0.2x ", data[i]);
			i++;

			if (pppStop||(i==len))
			{
				pppStop = false;
				return i;
			}
		}
		if (i>0)
		{
			printf("\n");
		}
	}
	return i;
}
//===========================================================================================================
// ������ �� LwIP � UART (GSM)
uint32_t sio_write(sio_fd_t fd, uint8_t *data, uint32_t len)
{
	uint32_t retLen = 0;
	if(uartParcerStruct.ppp.pppModeEnable)
	{
		if(HAL_UART_Transmit_IT(&huart1, data, len) == HAL_OK)
		{
			xSemaphoreTake(sioWriteSemaphore, portMAX_DELAY);
			retLen = len;
		}
		else
		{
			printf("HAL ERRROR WRITE [sio_write]\r\n");
		}
	}
	else
	{
		printf("sio_write not in PPP mode!\r\n");
	}

	return retLen;
}
//===========================================================================================================
// ���������� ������, ������� �������
void sio_read_abort(sio_fd_t fd)
{
	pppStop = true;
	xQueueReset(uartParcerStruct.uart.rxQueue);
}

//===========================================================================================================

uint32_t sys_jiffies(void) {
	return xTaskGetTickCount();
}
//===========================================================================================================
// ������ ���������� ����� �������� ��� �� �������� ������� ����������
static void linkStatusCB(void * ctx, int errCode, void * arg)
{
	printf("GSMPP: linkStatusCB\r\n"); /* just wait */
	bool *connected = (bool*)ctx;
	struct ppp_addrs * addrs = arg;

	switch (errCode)
	{
		case PPPERR_NONE:
			{ /* We are connected */
				//printf("ip_addr = %s\r\n", inet_ntoa(addrs->our_ipaddr));
				//printf("netmask = %s\r\n", inet_ntoa(addrs->netmask));
				//printf("dns1 = %s\r\n", inet_ntoa(addrs->dns1));
				//printf("dns2 = %s\r\n", inet_ntoa(addrs->dns2));
				*connected = 1;
				break;
			}
		case PPPERR_CONNECT:
			{
				printf("lost connection\r\n"); /* just wait */
				*connected = 0;
				break;
			}
		default:
			{ /* We have lost connection */
				printf("connection error\r\n"); /* just wait */
				*connected = 0;
				break;
			}
	}
}

//==============================================================================================================
// ����� ������
uint16_t getRxData(uint8_t serviceNum, xSemaphoreHandle xRxPppData, uint8_t **ppBufPacket)
{
     uint16_t retLen = 0;
     uint16_t size = 0;

	 if(xSemaphoreTake(xRxPppData, 1000/portTICK_PERIOD_MS) == pdTRUE)
	 {
          size = gsmLLR_TcpGetRxCount(serviceNum);

		  if(size > 1512)
		  {
			retLen = 0;
          }
		  else
		  {
			retLen = GsmLLR_TcpReadData(serviceNum, ppBufPacket, size);
          }
     }
return retLen;
}
