#include "modem.h"


volatile bool pppIsOpen = false;

volatile GsmState gsmState = {  .init = false,
                                .initLLR = false,
                                .initLLR2 = false,
                                .notRespond = false
                                };

ConnectionSettings connectionSettings = {   .gsmSettings.gprsPass = "",
                                            .gsmSettings.gprsUser = ""
                                            };


//�������� PPP.

//��� ������ ������ ������������ 4 ������� � comPPP_0-4.
//��� ��� ������������ � ����������� ����� �� �� �������������,
//��� ���� ��� ��������� ������. ���������� ���� � ����� ����:

char *comPPP_0[] = {"AT+CGDCONT=1,\"IP\","};
char *comPPP_2[] = {"AT+CGQMIN=1,0,0,0,0,0"};
char *comPPP_3[] = {"AT+CGQREQ=1,2,4,3,6,31"};
char *comPPP_4[] = {"ATD*99***1#"};

eRetComm GsmLLR_StartPPP(sGsmSettings *pSettings) {

	printf("StartPPP\r\n");
	//sResultCommand resultCommand;
	char *resultCommand[8];
	char **comPPP_Mass[3] = {comPPP_2, comPPP_3, comPPP_4};
	uint8_t *pData = NULL;

	//if(GsmLLR_GetMutex() == true) {
	if(xSemaphoreTake(GsmLLRMutex, portMAX_DELAY)== true) {

	pData = pvPortMalloc(GSM_MALLOC_COMMAND_SIZE);

		if(pData != NULL)
		{
			memset(pData, 0, GSM_MALLOC_COMMAND_SIZE);
			//void * memset( void * memptr, int val, size_t num );
			//������� memset ��������� num ������ ����� ������, ����� ��������� memptr. ��� ������������ ������� ��������� � ������� ����� �������� val.

			//sprintf((char*)pData, "%s%s", comPPP_0[0], (char*)pSettings->gprsApn);

			sprintf((char*)pData, "%s%s", comPPP_0[0], "internet");

			memset(resultCommand, 0, 8);
			RunAtCommand((char*)pData, (char *)&resultCommand);
			// ������� ������, ���� �� ��������� ���
			uint8_t stepIndex = 0;

			while(stepIndex != (3))
			{
				//uint16_t len = strlen((char*)*comPPP_Mass[stepIndex]);
				sprintf((char*)pData, "%s", (char*)*comPPP_Mass[stepIndex]);
				memset(resultCommand, 0, 8);
				RunAtCommand((char*)pData, (char*)&resultCommand);
				stepIndex++;
			}

			memset(pData, 0, GSM_MALLOC_COMMAND_SIZE);
			vPortFree(pData);
		}
		xSemaphoreGive(GsmLLRMutex);

	}
	return eOk;
}
//========================================================================================

//����� �������, ���������� � PPP

//Connect-Disconnect, ������ ������� ����������, �������� � �.�.

bool GsmPPP_Connect(uint8_t numConnect, char *pDestAddr, uint16_t port)
{

	struct ip4_addr resolved = {0};
	bool useDns = false;
	int32_t ipCut[4] = {0};

	if(!pppIsOpen)
	{
		printf("GSMPPP: CONNECT ERROR - PPP closed\r\n");
		return false;
	}

	sscanf(pDestAddr, "%i.%i.%i.%i", &ipCut[0], &ipCut[1], &ipCut[2], &ipCut[3]);

	if((ipCut[0]!=0)&&(ipCut[1]!=0)&&(ipCut[2]!=0)&&(ipCut[3]!=0))
	{
		IP4_ADDR(&connectionPppStruct.ipRemoteAddr[numConnect], ipCut[0],ipCut[1],ipCut[2],ipCut[3]); //31,10,4,158);
		useDns = false;
	}
	else
	{
		useDns = true;
	}

	if(connectionPppStruct.connected[numConnect] == false)
	{
		connectionPppStruct.tcpClient[numConnect] = tcp_new();	// create tcpPcb
		tcp_recv(connectionPppStruct.tcpClient[numConnect], server_recv);

		if(useDns == true)
		{/*
			switch(dns_gethostbyname(pDestAddr, &resolved, destServerFound, &numConnect))
			{
				case ERR_OK: // numeric or cached, returned in resolved
							connectionPppStruct.ipRemoteAddr[numConnect].addr = resolved.addr;
							break;

				case ERR_INPROGRESS: // need to ask, will return data via callback
							if(xSemaphoreTake(connectionPppStruct.semphr[numConnect], 10000/portTICK_PERIOD_MS) != pdTRUE)
							{
								while(tcp_close(connectionPppStruct.tcpClient[numConnect]) != ERR_OK)
								{
									vTaskDelay(100/portTICK_PERIOD_MS);
								}

								connectionPppStruct.connected[numConnect] = false;
								printf("GSMPPP: dns-ERROR\r\n");
								return false;
							}
							else
							{ }
							break;
			}*/
		}

		tcp_connect(connectionPppStruct.tcpClient[numConnect], &connectionPppStruct.ipRemoteAddr[numConnect], port, &TcpConnectedCallBack);

		if(xSemaphoreTake(connectionPppStruct.semphr[numConnect], 10000/portTICK_PERIOD_MS) == pdTRUE)
		{
			connectionPppStruct.connected[numConnect] = true;
			printf("GSMPPP: connected %s\r\n", inet_ntoa(connectionPppStruct.ipRemoteAddr));
			return true;
		}
		else
		{
			tcp_abort(connectionPppStruct.tcpClient[numConnect]);//tcp_close(connectionPppStruct.tcpClient[numConnect]);

			while(tcp_close(connectionPppStruct.tcpClient[numConnect]) != ERR_OK)
			{
				vTaskDelay(100/portTICK_PERIOD_MS);
			}

			printf("GSMPPP: connectTimeout-ERROR\r\n");
			return false;
		}
	}
	else
	{
		if(GsmLLR_ConnectServiceStatus(numConnect) == eOk)
		{
			printf("GSMPPP: CONNECT-already connected %s\r\n", inet_ntoa(connectionPppStruct.ipRemoteAddr));
			return true;
		}
		else
		{
			printf("GSMPPP: CONNECT CLOSE!!!\r\n");
			return false;
		}
	}
	return false;

}

//===========================================================================================================

bool GsmPPP_Disconnect(uint8_t numConnect)
{
	if(!pppIsOpen)
	{
		printf("GSMPPP: CONNECT ERROR - PPP closed\r\n");
		return false;
	}

	if(connectionPppStruct.tcpClient[numConnect] == NULL) {
		return false;
	}

	while(tcp_close(connectionPppStruct.tcpClient[numConnect]) != ERR_OK)
	{
		vTaskDelay(100/portTICK_PERIOD_MS);
	}

	connectionPppStruct.connected[numConnect] = false;
	return true;
}

//===========================================================================================================

bool GsmPPP_ConnectStatus(uint8_t numConnect)
{
	if(!pppIsOpen) {
		printf("GSMPPP: CONNECT ERROR - PPP closed\r\n");
		return false;
	}

	if(connectionPppStruct.tcpClient[numConnect]->state == ESTABLISHED) {
		return true;
	}

	return false;
}

//===========================================================================================================

bool GsmPPP_SendData(uint8_t numConnect, uint8_t *pData, uint16_t len) {

	if(!pppIsOpen)
	{
		printf("GSMPPP: CONNECT ERROR - PPP closed\r\n");
		return false;
	}

	if(tcp_write(connectionPppStruct.tcpClient[numConnect], pData, len, NULL) == ERR_OK)
	{
		return true;
	}
	else
	{
		while(tcp_close(connectionPppStruct.tcpClient[numConnect]) != ERR_OK)
		{
			vTaskDelay(100/portTICK_PERIOD_MS);
		}

		connectionPppStruct.connected[numConnect] = false;
		connectionPppStruct.rxData[numConnect].rxBufferLen = 0;
		memset(connectionPppStruct.rxData[numConnect].rxBuffer,0, sizeof(connectionPppStruct.rxData[numConnect].rxBuffer));
	}

	return false;
}

//===========================================================================================================

uint16_t GsmPPP_GetRxLenData(uint8_t numConnect) {

	if(!pppIsOpen)
	{
		printf("GSMPPP: CONNECT ERROR - PPP closed\r\n");
		return false;
	}

	return connectionPppStruct.rxData[numConnect].rxBufferLen;
}

//===========================================================================================================

uint16_t GsmPPP_ReadRxData(uint8_t numConnect, uint8_t **ppData)
{
	if(!pppIsOpen)
	{
		printf("GSMPPP: CONNECT ERROR - PPP closed\r\n");
		return false;
	}

	if(connectionPppStruct.rxData[numConnect].rxBufferLen != 0)
	{
		*ppData = (uint8_t *) connectionPppStruct.rxData[numConnect].rxBuffer;
		uint16_t retLen = connectionPppStruct.rxData[numConnect].rxBufferLen;
		connectionPppStruct.rxData[numConnect].rxBufferLen = 0;
		return retLen;
	}

	return false;
}

//===========================================================================================================

static void destServerFound(const char *name, struct ip4_addr *ipaddr, void *arg)
{
	uint8_t *num = (uint8_t*)arg;

	if(*num < SERVERS_COUNT)
	{
		printf("GSMPPP: DEST FOUND %s\r\n", inet_ntoa(ipaddr->addr));
		connectionPppStruct.ipRemoteAddr[*num].addr = ipaddr->addr;
		xSemaphoreGive(connectionPppStruct.semphr[*num]);
	}
	else
	{
		printf("GSMPPP: DNS != SERVER%s\r\n", inet_ntoa(ipaddr->addr));
	}
}

//===========================================================================================================

static err_t TcpConnectedCallBack(void *arg, struct tcp_pcb *tpcb, err_t err)
{
	for(uint8_t i=0; i<SERVERS_COUNT; i++)
	{
		if(tpcb == connectionPppStruct.tcpClient[i])
		{
			printf("GSMPPP: connected (callback)%s\r\n", inet_ntoa(tpcb->local_ip.addr));
			xSemaphoreGive(connectionPppStruct.semphr[i]);
			break;
		}
	}
}

//===========================================================================================================

static err_t server_recv(void *arg, struct tcp_pcb *pcb, struct pbuf *p, err_t err)
{
   LWIP_UNUSED_ARG(arg);

	if(err == ERR_OK && p != NULL)
	{
		tcp_recved(pcb, p->tot_len);
		printf("GSMPPP:server_recv(): pbuf->len %d byte\n [%s]", p->len, inet_ntoa(pcb->remote_ip.addr));

		for(uint8_t i=0; i<SERVERS_COUNT; i++)
		{
			if(pcb->remote_ip.addr == connectionPppStruct.tcpClient[i]->remote_ip.addr)
			{
				printf("GSMPPP: server_recv (callback) [%s]\r\n", inet_ntoa(pcb->remote_ip.addr));
				if(p->len < sizeof(connectionPppStruct.rxData[i].rxBuffer))
				{
					memcpy(connectionPppStruct.rxData[i].rxBuffer, p->payload, p->len);
					connectionPppStruct.rxData[i].rxBufferLen = p->len;
					xSemaphoreGive(connectionPppStruct.rxData[i].rxSemh);
					printf("GSMPPP: server_recv (callback) GIVE SEMPH[%s][%d]\r\n", inet_ntoa(pcb->remote_ip.addr), p->len);
				}
				else
				{
					printf("GSMPPP: server_recv p->len > sizeof(buf) -ERROR\r\n");
				}
			}
		}

    pbuf_free(p);
	}
	else
	{
		printf("\nserver_recv(): Errors-> ");
		if (err != ERR_OK)
				printf("1) Connection is not on ERR_OK state, but in %d state->\n", err);
		if (p == NULL)
				printf("2) Pbuf pointer p is a NULL pointer->\n ");

		printf("server_recv(): Closing server-side connection...");

		pbuf_free(p);

		server_close(pcb);
	}
   return ERR_OK;
}

//===========================================================================================================

xSemaphoreHandle * GsmPPP_GetRxSemaphorePoint(uint8_t numService) {
	return (connectionPppStruct.rxData[numService].rxSemh);
}

//===========================================================================================================


//������� ��������� � ����������� TCP � LwIP, callback-s

static err_t server_poll(void *arg, struct tcp_pcb *pcb)
{
   static int counter = 1;
   LWIP_UNUSED_ARG(arg);
   LWIP_UNUSED_ARG(pcb);
   printf("\nserver_poll(): Call number %d\n", counter++);
   return ERR_OK;
}
//===========================================================================================================
static err_t server_err(void *arg, err_t err)
{
   LWIP_UNUSED_ARG(arg);
   LWIP_UNUSED_ARG(err);
   printf("\nserver_err(): Fatal error, exiting...\n");
   return ERR_OK;
}
//===========================================================================================================
static void server_close(struct tcp_pcb *pcb)
{
	tcp_arg(pcb, NULL);
	tcp_sent(pcb, NULL);
	tcp_recv(pcb, NULL);
	while(tcp_close(pcb) != ERR_OK)
	{
		vTaskDelay(100/portTICK_PERIOD_MS);
	}

	for(uint8_t i=0; i<SERVERS_COUNT; i++)
	{
		if(pcb == connectionPppStruct.tcpClient[i])
		{
			printf("GSMPPP: server_close (callback)%s\r\n", inet_ntoa(pcb->local_ip.addr));
			connectionPppStruct.connected[i] = false;
		}
		else
		{
			printf("GSMPPP: server_recv p->len > sizeof(buf) -ERROR\r\n");
		}
	}
}
//===========================================================================================================

void RunAtCommand(char * pData, char * resultCommand){



}

//===========================================================================================================

uint16_t gsmLLR_TcpGetRxCount(uint8_t serviceNum){

return 0;

}
//===========================================================================================================
uint16_t GsmLLR_TcpReadData(uint8_t serviceNum, uint8_t ** ppBufPacket, uint16_t size){

return 0;

}
//===========================================================================================================
bool GsmLLR_ATAT(){return false;}
//===========================================================================================================
bool GsmLLR_WarningOff(){return false;}
//===========================================================================================================
bool GsmLLR_FlowControl(){return false;}
//===========================================================================================================
bool GsmLLR_AtCREG(){return false;}
//===========================================================================================================
xSemaphoreHandle GsmLLR_GetRxSemphorePoint(uint8_t serviceNum){return NULL;}
//===========================================================================================================
bool GsmLLR_ConnectServiceStatus(uint8_t serviceNum){return false;}
//===========================================================================================================
bool GsmLLR_DisconnectService(uint8_t serviceNum){return false;}
//===========================================================================================================
bool GsmLLR_ConnectService(uint8_t serviceNum){return false;}
//===========================================================================================================
bool GsmLLR_ModuleLost(){return false;}
//===========================================================================================================
bool GsmLLR_TcpSend(uint8_t serviceNum, uint8_t* rxBuff, uint16_t rxLen){return false;}
