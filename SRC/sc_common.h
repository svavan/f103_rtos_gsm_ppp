#ifndef SC_COMMON_H_INCLUDED
#define SC_COMMON_H_INCLUDED

#include "stdint.h"
#include "stdbool.h"
#include "stm32f1xx_hal.h"
#include "cmsis_os.h"
#include "sio.h"
//#include "projdefs.h"
//#include "stm32f1xx_hal_def.h"
//#include "portmacro.h"
#include "queue.h"
#include "ppp.h"
#include "modem.h"

bool pppStop;

volatile xSemaphoreHandle sioWriteSemaphore;

typedef struct{

    QueueHandle_t rxQueue;
    bool receiveState;

}
Uart;

typedef struct{

    bool pppModeEnable;

}
PPP;

typedef struct{

    PPP ppp;
    Uart uart;

}
UartParcerStruct;

UartParcerStruct uartParcerStruct;

static void linkStatusCB(void * ctx, int errCode, void * arg);

#endif /* SC_COMMON_H_INCLUDED */
