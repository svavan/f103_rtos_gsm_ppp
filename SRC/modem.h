#ifndef MODEM_H_INCLUDED
#define MODEM_H_INCLUDED

#include "stdbool.h"
//#include "FreeRTOS.h"
#include "cmsis_os.h"
#include "ip_addr.h"
#include "sio.h"
#include "err.h"
#include "tcp.h"
#include "ip4_addr.h"
#include "string.h"


#define eOk true
#define GSM_MALLOC_COMMAND_SIZE 32
#define CONNECTION_PPP_COUNT 1
#define SERVERS_COUNT 1


typedef bool eRetComm;
typedef char* sGsmSettings;

volatile xSemaphoreHandle GsmLLRMutex;


typedef struct
{
    uint16_t rxBufferLen;
    uint8_t * rxBuffer;
    volatile xSemaphoreHandle rxSemh;
}
RxData;


typedef struct
{
    ip4_addr_t ipRemoteAddr[CONNECTION_PPP_COUNT];
    bool connected[CONNECTION_PPP_COUNT];
    struct tcp_pcb* tcpClient[CONNECTION_PPP_COUNT];
    volatile xSemaphoreHandle semphr[CONNECTION_PPP_COUNT];
    RxData rxData[CONNECTION_PPP_COUNT];
}
ConnectionPppStruct;

typedef struct
{
     bool init;
     bool initLLR;
     bool initLLR2;
     bool notRespond;
}
GsmState;

typedef struct
{
    char* gprsUser;
    char* gprsPass;
}
GsmSettings;


typedef struct
{
    GsmSettings gsmSettings;
}
ConnectionSettings;

ConnectionSettings connectionSettings;

ConnectionPppStruct connectionPppStruct;

bool GsmLLR_WarningOff();
bool GsmLLR_FlowControl();

bool GsmLLR_AtCREG();
bool GsmLLR_StartPPP();

bool GsmLLR_Init();
bool GsmLLR2_Init();
bool GsmPPP_Init();

bool GsmLLR_PowerUp();
bool GsmLLR_ModuleLost();
bool GsmLLR_ATAT();

bool GsmLLR_GetMutex();

uint16_t gsmLLR_TcpGetRxCount(uint8_t serviceNum);
uint16_t GsmLLR_TcpReadData(uint8_t serviceNum, uint8_t ** ppBufPacket, uint16_t size);

xSemaphoreHandle GsmLLR_GetRxSemphorePoint(uint8_t serviceNum);

bool GsmLLR_TcpSend(uint8_t serviceNum, uint8_t* rxBuff, uint16_t rxLen);

bool GsmLLR_ConnectServiceStatus(uint8_t serviceNum);

bool GsmLLR_DisconnectService(uint8_t serviceNum);

bool GsmLLR_ConnectService(uint8_t serviceNum);

eRetComm GsmLLR_StartPPP(sGsmSettings *pSettings);

void RunAtCommand(char * pData, char * resultCommand);

static err_t server_recv(void *arg, struct tcp_pcb *pcb, struct pbuf *p, err_t err);
static err_t TcpConnectedCallBack(void *arg, struct tcp_pcb *tpcb, err_t err);
static void server_close(struct tcp_pcb *pcb);

#endif /* MODEM_H_INCLUDED */
