#ifndef USER_TASKS_H_INCLUDED
#define USER_TASKS_H_INCLUDED

#include "cmsis_os.h"
//#include "stdio.h"
#include "stdbool.h"
#include "modem.h"
#include "ppp.h"
#include "sc_common.h"

// ���-�� ����������. � ���� ������� 1, ����� ���� ������, ���������� �������� RAM ����������� �
//������������� LwIP (��������� ����� �������������)
#define GSM_MAX_CONNECTION	1
#define GSM_CONNECTION_ERROR_DELAY 1000

// ��������� ��� ������ � �������

extern GsmState gsmState;
extern bool pppIsOpen;

typedef struct
{
     uint8_t *rxBuff;
     uint16_t rxLen;
}
sBuff[GSM_MAX_CONNECTION];

void StartThread(void * argument);
void gsmTaskInit(void);
void connectTask(void *pServiceNum);
void vGsmTask( void * pvParameters );


#endif /* USER_TASKS_H_INCLUDED */
